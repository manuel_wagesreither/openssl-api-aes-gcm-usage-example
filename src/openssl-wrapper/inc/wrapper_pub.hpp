#pragma once

// Own header
#include <wrapper_types.hpp>

namespace ossl {

encrypted_pair encrypt(
	const octet_string& plaintext,
	const octet_string& aad,
	const octet_string& key,
	const octet_string& iv
);


octet_string decrypt(
	const octet_string& ciphertext,
	const octet_string& aad,
	const octet_string& tag,
	const octet_string& key,
	const octet_string& iv
);

} // namespace
