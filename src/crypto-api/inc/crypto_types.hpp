#pragma once
#include "wrapper_types.hpp"

namespace crypto {

class Message
{
	const ossl::octet_string _ciphertext;
	const ossl::octet_string _tag;
	const ossl::octet_string _aad;
	const ossl::octet_string _iv;

public:
	Message(
		ossl::octet_string ciphertext,
		ossl::octet_string tag,
		ossl::octet_string aad,
		ossl::octet_string iv) :
	_ciphertext{ciphertext},
	_tag{tag},
	_aad{aad},
	_iv{iv}
	{};

	ossl::octet_string ciphertext() const { return _ciphertext; };
	ossl::octet_string tag() const { return _tag; };
	ossl::octet_string aad() const { return _aad; };
	ossl::octet_string iv() const { return _iv; };
};

} // namespace
