#include <iostream>
#include <cstring>
#include <cstdlib>
#include "crypto_pub.hpp"


bool valid(int& argc, char*& progname) {
	if (argc == 2) {
		return true;
	} else {
		std::cerr
			<< progname << ": "
			<< "Error: Please provide plaintext as first argument"
			<< std::endl;
		return false;
	}
}


bool equal(
	const std::string& plaintext,
	const std::string& decrypted_ciphertext,
	char* progname
){
	if (plaintext == decrypted_ciphertext) {
		return true;
	} else {
		std::cerr << progname << ": "
<< "Error: Decrypted ciphertext differs from unencrypted plaintext"
			<< std::endl;
		return false;
	}
}


int main(int argc, char** argv)
{
	// Check supplied arguments
	if (!valid(argc, argv[0]))
		return EXIT_FAILURE;

	// Convert to a more convenient type
	const std::string plaintext( argv[1] );

	// Encrypt
	std::cout << "Unencrypted plaintext: " << plaintext << std::endl;
	const crypto::Message encrypted = crypto::encrypt( plaintext );

	// Decrypt
	const std::string decrypted = crypto::decrypt( encrypted );
	std::cout << "Decrypted ciphertext: " << decrypted << std::endl;

	// Check for encryption/decrytion correctnes
	if ( !equal( plaintext, decrypted, argv[0] ))
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}
