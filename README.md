This project demonstrates how to use OpenSSL and an AES-128 GCM key to encrypt
and decrypt data.

The program takes
* the plaintext to encrypt as CLI parameter, and
* additional authenticated data (AAD) (that is, data that is signed but not
encrypted) as a global variable,

and then calculates
* ciphertext and
* tag value

from it using a key which is defined as a global variable.

Next, the same key is used to take
* ciphertext,
* tag value, and
* initialization vector

and re-create the original plaintext, which is then printed on stdout.

This program was created for me to remember the exact steps (that is, the
OpenSSL API functions to call) to accomplish this task. It is my personal
reference guide and no guarantee is given whatsover.

How to build in GNU/Linux
===

You need `cmake`, `make` and a C++ compiler set up.

From within the project directory, run:

```
# mkdir build
# cd build
# cmake ..
# make
```

To see the exact compiler invocation commands, replace the last step with
`make VERBOSE=1`.
