// Own header
#include "wrapper.hpp"

// stdlib headers
#include <utility>
#include <stdexcept>

// system headers
#include <openssl/evp.h>

#define TAG_LEN           16

namespace ossl {

encrypted_pair encrypt(
	const octet_string & plaintext,
	const octet_string & aad,
	const octet_string & key,
	const octet_string & iv
){
	int rc;

	/* Create and initialise the context */
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	if (ctx == NULL) {
		char msg[] = "EVP_CIPHER_CTX_new() failed";
		throw std::runtime_error(msg);
	}

	// Deactivate padding. 0 stands for "false", not for padding length.
	rc = EVP_CIPHER_CTX_set_padding(ctx, 0);
	if (!rc) {
		char msg[] = "EVP_CIPHER_CTX_set_padding() failed";
		throw std::runtime_error(msg);
	}

	// Set engine impl., key and iv
	rc = EVP_EncryptInit_ex(ctx,
		EVP_aes_128_gcm(),
		NULL,
		NULL,
		NULL
	);
	if (!rc) {
		char msg[] = "Calling EVP_EncryptInit_ex() failed";
		throw std::runtime_error(msg);
	}

	// Set the iv length; default is '12'
	rc = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv.length(), NULL);
	if (!rc) {
		char msg[] = "Setting iv length through EVP_CIPHER_CTX_ctrl() failed";
		throw std::runtime_error(msg);
	}

	// Set engine impl., key and iv
	rc = EVP_EncryptInit_ex(ctx,
		NULL,
		NULL,
		key.data(),
		iv.data()
	);
	if (!rc) {
		char msg[] = "Calling EVP_EncryptInit_ex() failed";
		throw std::runtime_error(msg);
	}

	int output_len;

	// Load additional authenticated data
	rc = EVP_EncryptUpdate(ctx,
		NULL,
		&output_len,
		(unsigned char*)aad.data(),
		aad.length()
	);
	if ((unsigned int)output_len != aad.length()) {
		char msg[] = "EVP_EncryptUpdate() failed for loading additional authenticated data";
		throw std::runtime_error(msg);
	}

	// We're deactivating padding, so ciphertext length
	// must be equal to plaintext length.
	unsigned char *output_arr = new unsigned char[plaintext.length()];

	// Provide the message to be encrypted, and obtain the encrypted output.
	// EVP_EncryptUpdate can be called multiple times if necessary
	rc = EVP_EncryptUpdate(ctx,
		output_arr,
		&output_len,
		plaintext.data(),
		plaintext.length()
	);
	if (!rc) {
		char msg[] = "EVP_EncryptUpdate() failed";
		throw std::runtime_error(msg);
	}

	octet_string ciphertext(output_arr, output_len);

	// Finalize the encryption. As we are not using padding, this should
	// not write any additional bytes.
	rc = EVP_EncryptFinal_ex(ctx,
		output_arr,
		&output_len
	);
	if (!rc) {
		char msg[] = "EVP_EncryptFinal_ex() failed";
		throw std::runtime_error(msg);
	}
	if (output_len != 0) {
		char msg[] = "EVP_EncryptFinal_ex() unexpectedly wrote additional bytes";
		throw std::runtime_error(msg);
	}

	// Prepare the output array
	unsigned char tag_arr[TAG_LEN];

	// Get the tag
	rc = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG,	TAG_LEN, tag_arr);
	if (!rc) {
		char msg[] = "Getting tag through EVP_CIPHER_CTX_ctrl()";
		throw std::runtime_error(msg);
	}
	octet_string tag(tag_arr, TAG_LEN);

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);
	delete[] output_arr;

	return encrypted_pair(ciphertext, tag);
}


octet_string decrypt(
	const octet_string & ciphertext,
	const octet_string& aad,
	const octet_string & tag,
	const octet_string & key,
	const octet_string & iv
){
	int rc;

	/* Create and initialise the context */
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	if (ctx == NULL) {
		char msg[] = "EVP_CIPHER_CTX_new() failed";
		throw std::runtime_error(msg);
	}

	// We don't use padding. 0 stands for 'false', not for padding length
	rc = EVP_CIPHER_CTX_set_padding(ctx, 0);
	if (!rc) {
		char msg[] = "EVP_CIPHER_CTX_set_padding() failed";
		throw std::runtime_error(msg);
	}

	// Initialize engine implementation and key
	rc = EVP_DecryptInit_ex(ctx,
		EVP_aes_128_gcm(),
		NULL,
		key.data(),
		NULL
	);
	if (!rc) {
		char msg[] = "EVP_DecryptInit_ex() failed";
		throw std::runtime_error(msg);
	}

	// Set the expected tag value
	rc = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, TAG_LEN, (unsigned char*)tag.data());
	if (!rc) {
		char msg[] = "Setting tag through EVP_CIPHER_CTX_ctrl() failed";
		throw std::runtime_error(msg);
	}

	// IV length can be set only when cipher (EVP_aes_128_gcm) is set
	rc = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv.size(), NULL);
	if (!rc) {
		char msg[] = "Setting iv length through EVP_CIPHER_CTX_ctrl() failed";
		throw std::runtime_error(msg);
	}

	// Initialization of IV must be done after setting its length
	rc = EVP_DecryptInit_ex(ctx,
		NULL,
		NULL,
		NULL,
		iv.data()
	);
	if (!rc) {
		char msg[] = "EVP_DecryptInit_ex() failed";
		throw std::runtime_error(msg);
	}

	int output_len;

	// Load additional authenticated data
	rc = EVP_DecryptUpdate(ctx,
		NULL,
		&output_len,
		aad.data(),
		aad.size()
	);
	if (output_len != aad.size()) {
		char msg[] = "EVP_DecryptUpdate() failed for loading additional authenticated data";
		throw std::runtime_error(msg);
	}

	// We're not using padding, so the plaintext length should
	// equal the ciphertext length
	unsigned char *output_arr = new unsigned char[ciphertext.size()];

	// Provide the message to be decrypted, and obtain the plaintext output.
	// This function could be called several times if necessary
	// We're not using padding, hence plaintext length should equal the ciphertext length
	rc = EVP_DecryptUpdate(ctx,
		output_arr,
		&output_len,
		ciphertext.data(),
		ciphertext.size()
	);
	if (!rc) {
		char msg[] = "EVP_DecryptUpdate() failed for loading ciphertext";
		throw std::runtime_error(msg);
	}
	if ((unsigned int)output_len != ciphertext.size()) {
		char msg[] = "Plaintext length unequals ciphertext length";
		throw std::runtime_error(msg);
	}

	octet_string plaintext(output_arr, output_len);

	// Finalise the decryption. A positive return value indicates success,
	// anything else is a failure - the plaintext is not trustworthy.
	// This would write remaining bytes. As we're not using padding, it
	// should not write anything.
	rc = EVP_DecryptFinal_ex(ctx,
		output_arr,
		&output_len
	);
	if (!rc) {
		char msg[] = "EVP_DecryptFinal_ex() failed";
		throw std::runtime_error(msg);
	}
	if (output_len != 0) {
		char msg[] = "EVP_DecryptFinal_ex() unexpectedly wrote additional bytes";
		throw std::runtime_error(msg);
	}

	// Clean up
	EVP_CIPHER_CTX_free(ctx);
	delete[] output_arr;

	return plaintext;
}

} // namespace