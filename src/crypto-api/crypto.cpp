// Own header
#include "crypto.hpp"

// Project headers
#include "helper_pub.hpp"
#include "wrapper_pub.hpp"

namespace crypto {

Message encrypt( const std::string& plaintext )
{
	auto result = ossl::encrypt(
		ossl::octet_string( plaintext ),
		config::get_aad(), // Additional authenticated data
		config::get_key(),
		config::get_iv() // Initialization vector
	);

	return Message(
		// The actual payload, encrypted
		result.ciphertext(),
		//
		// Needs to go with the encrypted payload.
		// I don't know why.
		result.tag(),
		//
		// Additional authenticated data
		config::get_aad(), 
		//
		// BUG: Initialization vector is constant
		// and therefore breaks security.
		config::get_iv() 
	);
}


std::string decrypt( const Message& message )
{
	auto decrypted = ossl::decrypt(
		message.ciphertext(),
		message.aad(),
		message.tag(),
		config::get_key(),
		message.iv()
	);

	return decrypted.std_str();
}

} // namespace
