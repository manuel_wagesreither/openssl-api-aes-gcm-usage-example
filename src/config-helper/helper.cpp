// Own header
#include "helper.hpp"

// Project headers
#include "config.hpp"


namespace config {

// was const
ossl::octet_string get_aad()
{
	return ossl::octet_string(aad_raw, sizeof(aad_raw));
}


// was not const
ossl::octet_string get_key()
{
	return ossl::octet_string(key_raw, sizeof(key_raw));
}


// was const
ossl::octet_string get_iv()
{
	return ossl::octet_string(iv_raw, sizeof(iv_raw));
}

} // namespace